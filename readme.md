

# FAPSeal断层封闭性评价系统使用手册

## 编写目的
本文档是FAPSeal断层封闭性分析软件针对用户所编写的使用说明手册，本文档中通过对FAPSeal断层封闭性分析软件进行了详细而具体的操作描述，通过该文档读者可以了解系统的所有功能以及具体操作。

## 背景
目前，在非常规油气勘探和开发、地热能源开发、气体与核废料地下埋存等能源探采领域，断层和小尺度破裂构造的几何学形态、封闭特性和稳定性问题越发凸显。我公司以断层相关问题为导向，通过智能化机器学习方法、高效的人机交互方式、独有的断层物性数据库，建立了断层封闭性和稳定性评价软件，旨在为用户提供更符合实际地质条件和更加高效的断层封闭性和稳定性评价方式。





## 本文档使用 Material for MkDoc 构建

For full documentation visit [mkdocs.org](https://www.mkdocs.org).

### 安装

需要 Python 和 Python package manager pip 来安装 MkDocs . MkDocs 支持 Python 2.6, 2.7, 3.3 和 3.4.

使用 pip 安装 mkdocs :
```
$ pip install mkdocs
```
mkdocs已经安装到你的系统. 运行 mkdocs help 以检查是否正确安装.

输入以下命令以开始一个新项目.
```
$ mkdocs new my-project
```

主题：material 

https://squidfunk.github.io/mkdocs-material/

```
pip install mkdocs-material
```
图片缩放功能
```
pip install mkdocs-glightbox
```

高级目录导航排序
```
pip install mkdocs-awesome-pages-plugin
```
Git管理
```
pip install mkdocs-git-revision-date-localized-plugin
```
### 命令

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs -h` - Print help message and exit.

### Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        断层稳定性/断层稳定性原理及影响因素.md
        断层稳定性/断层稳定性评价方法.md
        断层稳定性/盖层完整性/盖层水力封闭能力/盖层水力封闭能力机理.md
        ...       # Other markdown pages, images and other files.
    home.html
        {% block content %}{% endblock %}
        {% block footer %}{% endblock %}