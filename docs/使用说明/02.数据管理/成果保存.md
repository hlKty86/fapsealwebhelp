成果是本软件报告生成的重要一项，成果维护包括成果的保存、成果另存为、成果加载、成果删除等功能。

* 成果保存：点击快捷工具栏的保存，弹出保存成果的设置框，输入成果名称和所在分组，如图所示。

![成果保存](../media/59373c8b37f5cc4b41931d8450f1b55f.png)


* 成果加载，成果保存后，在成果视图列表中会出现之前保存的成果，如图所示。双击成果名称，即可加载成果，如图所示。

![成果视图](../media/64947a198d0a594a9e22e80d159e42e1.png)



![成果加载](../media/12007eb96cb5175759d714fba43133c3.png)



* 成果另存，如图所示。

![成果另存](../media/449d898139f0f4c4c5d2f7ed3c08e4f1.png)



* 成果删除：右键成果栈成果名称，点击删除即可。

* 成果编辑：编辑成果名称，右键成果栈成果名称，选择编辑，如图所示。

![成果编辑](../media/0d842c054cc5b1579867eb1986a87bcf.png)



* 成果组管理，右键成果栈成果名称，选择分组管理，如图所示。

![成果分组管理](../media/f0f13ba99872fb89596e76a0d4ec6d45.png)