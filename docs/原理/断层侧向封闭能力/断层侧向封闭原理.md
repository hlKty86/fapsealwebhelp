# 断层侧向封闭原理

## 断层带内部结构

断层带是在构造应力的作用下以两侧岩体发生明显位移为特征，以周围岩体发生相应应变为产物，经历多期构造运动和流体流动改造的复杂三维地质体，结构上普遍发育断层核与破碎带。其中断层核是断层两侧岩体位移最为集中的部位，发育滑动面和断层岩。破碎带位于断层核两侧，主要为断层形成过程中伴生的次级构造，包括伴生褶皱构造、次级小断层、变形带、裂缝、端部过程带、连接部位的转换带。

![断层带内部结构模式图](%E6%96%AD%E5%B1%82%E5%B8%A6%E5%86%85%E9%83%A8%E7%BB%93%E6%9E%84%E6%A8%A1%E5%BC%8F%E5%9B%BE.jpg)

![断裂带内部结构（Caine et al., 1996; Faulkner et al., 2003）](%E6%96%AD%E8%A3%82%E5%B8%A6%E5%86%85%E9%83%A8%E7%BB%93%E6%9E%842.jpg)

### 断层核
断层核是指在断裂带内，较高地局部应变、调解大多数位移和强剪切共同作用的结果，其主要是由大量的破裂滑动面和断层岩组成。其中滑动面是由于吸收断层大部分位移造成的；而断层核内断层岩类型主要是取决于母岩性质及不同变形期内断裂变形机制，主要包括解聚带、碎裂岩、层状硅酸盐框架结构、泥岩涂抹以及胶结作用形成的断层岩（压溶型断层岩、胶结断层岩（方解石胶结、二氧化硅胶结和沥青塞等））。

### 破碎带 
空间上与某一断裂带有关的高裂缝岩石变形区都可以称为破碎带（Damage zone），破碎带的特点是与断层核相比具有低的应力和较少的强烈变形，并且破碎带内包含一些次级构造例如次级断裂、裂缝以及与断层箱管褶皱一样的纹理。在构造地质中关于破碎带的概念有两种略微不同的用法 ：一种是作为断裂带内部的一种结构，指分布在断层核或断面周围的高裂缝区	；另一种是指与断裂滑动或与断层端部活动和连接有关的，富含次生断裂、裂缝的变形区。此处所说的破碎带为第二种情况，Kim（2003）建立了一个三维概念模型 ，为我们划分破碎模式提供了基础，根据破碎带中次生断裂在断层周围位置和拓展模式，断层破碎带可分为四种类型：端部破碎带（tip damage），围岩破碎带（wall damage），连接破碎带（linkage damage）和分散破碎带（distributed damage）。 



??? tips "断层段周围破碎带类型"
    ![断层段周围破碎带类型](%E6%96%AD%E5%B1%82%E6%AE%B5%E5%91%A8%E5%9B%B4%E7%A0%B4%E7%A2%8E%E5%B8%A6%E7%B1%BB%E5%9E%8B.jpg)

    1. 端部破碎带：发育在断层的端部。较为容易识别，在文献中有广泛的记载，断层端部的伸展破裂在一些数值模拟研究中也有过描述。这类破碎带是断层端部应力集中及调节位移快速变化而产生。端部破碎带构造样式与晶体物质中裂缝拓展产生的过程带（processzone）相似，这种断裂很容易拓展成大规模的断层，成为大规模孕震断层，对地震断裂的成核和终止有重要影响。
    2. 围岩破碎带：裂缝存在于断层周围的围岩内，随着远离断层裂缝发育程度逐渐降低。断层生长的不同过程中围岩破碎带的发育特征不同，较难加以区分。围岩破碎分为三种类型：a、以Ⅱ型（滑移型）裂缝形式从端部拓展（即端部破碎带的持续发育）；b、以Ⅲ型（撕裂型）裂缝形式在端部相互作用；c、由于断层滑动引起的相关拖曳导致的围岩中应变增加而产生的破碎模式(运动学形式破碎带)。
    3. 连接破碎带：为发育于断层叠覆区的高密度裂缝	。连接破碎带可能由叠覆区的端部破碎带发育而来，也可能在受断层演化过程中调节叠覆部位的累积位移而产生。根据叠覆断层的相互位移情况（即转换带的应力环境），连接破碎带分为两大类：扩张弯曲（dilationaljogs）和压实弯曲（contractionaljogs）。
    4. 分散破碎带：这类裂缝主要分布在断层周围的岩石中，它们在主断层形成前就可能存在，并对断层和破碎带的发育产生一定影响（即Crider和Peacock，2004提出的前兆破碎）。可是，沿着断层分散破碎带和围岩破碎带的发育程度是相对较弱的。

## 不同类型断层岩泥质含量分布特征

### 解聚带 

解聚带是指当断层岩泥质含量小于15%时，将该类断层岩定义为解聚带。在断层变形期内，解决带是由纯净砂岩（母岩中泥质含量小于15%）卷入断层带内而形成的。解聚带的母岩是正常的砂岩和低固结砂岩	。在解聚过程中，由于断层变形期内岩石颗粒没有发生大规模的破裂，颗粒是以颗粒流的方式流动调节应力，因此，该类断层岩具有仅发生变形但不破裂的特征。同时解聚带的渗透率与母岩的渗透率具有相似性，侧向遮挡油气的能力很差。其根本原因是在解聚带内没有足够的泥岩或层状硅酸盐，来作为泥岩涂抹或胶结的来源。

### 碎裂岩 

碎裂岩是指当断层岩泥质含量小于15%时，将该类断层岩定义为碎裂岩。在断层变形期内，碎裂岩是由纯净砂岩（母岩中泥质含量小于15%）卷入断层带内而形成的。与解聚带的形成具有相似性，但是解聚带是岩石颗粒发生变形而不破裂，而碎裂岩则是指岩石颗粒发生碎裂变形。在碎裂作用过程中，由于颗粒破裂和摩擦产生的颗粒翻转，使颗粒尺寸变小，导致颗粒间孔隙度降低以及胶结的可能性。同时由于颗粒间摩擦作用使岩石颗粒发生翻转，进而致使颗粒沿剪切方向平行排列，在压实作用下更易降低该类断层岩的孔隙度。虽然与母岩相比碎裂岩的孔隙度有所降低，但碎裂岩侧向封闭油气的能力依旧较差。然而，当地温大于 90℃时，碎裂岩中石英颗粒可发生大规模的胶结，导致断层侧向封闭能力增强，可侧向遮挡油气聚集成藏。 

根据岩化作用情况，碎裂岩可被细分成三种类型：1.低岩化碎裂岩，在岩石颗粒间很少甚至没有压实或胶结作用，以及岩石颗粒间呈点接触；2.部分岩化碎裂岩，它存在着一定的压实和胶结；3.完全岩化的碎裂岩，呈现出颗粒连接现象，这是由于后期变形溶解或胶结造成的。 

### 层状硅酸盐框架断层岩

层状硅酸盐-框架结构断层岩（PFFRs）是指当断层岩泥质含量在15%—40%之间时，将该类断层岩定义为层状硅酸盐-框架断层岩。在断层变形期内，不纯净砂岩（母岩中泥质含量在15%—40%之间）卷入断层带内，由不纯净砂岩中层状硅酸盐与框架结构硅酸盐混合而形成的，或者是高泥岩含量地层与低泥岩含量地层混合形成。与母岩孔渗性相比，该类断层岩的渗透率和孔隙度降低的非常明显。因此，层状硅酸盐-框架结构断层岩（PFFRs）侧向封闭性强，可遮挡油气聚集成藏。 

### 泥岩涂抹

泥岩涂抹是指当断层岩内泥质含量大于40%时，将该类断层岩定义为泥岩涂抹。在断层变形期内，由不纯净砂岩（母岩中泥质含量大于40%）在断层剪切作用下所形成的。在剪切环境下，进入断层带内的泥岩物质沿着断层面可形成低渗透率且连续的泥岩带。因此泥岩涂抹具有很强的侧向封闭油气的能力。

泥岩涂抹的影响因素主要有两种：一是地层中泥岩体积与分布；二是断层的断距大小。Knipe和 Ottesen Ellevset 等人研究表明一旦断距大小超过进入断层带内地层厚度的3倍时，泥岩涂抹则不具有连续性，在断层面上将出现渗漏点，断层具有一定的封闭能力。 

### 胶结断层岩 
如果胶结的断层带大范围发育，基于断层岩泥质含量预测的断层封闭性分析可能是无效的。但是在许多情况下，胶结不能够广泛地影响断层带封闭性质，因为胶结很少能形成连续封闭，除非被限制在断裂带内有限的区域或者是在断移地层内有着胶结的趋势。一般地，胶结封闭仅仅是发生在断裂带内部，封闭性质主要是受矿物溶解-再重组过程或者是新的矿物沉淀形成所影响。因此，胶结封闭是与在变形期内溶解和重组的位置有关，或者是与断裂内流体侵入路径有关。

对于胶结的断裂和裂缝，Knipe等人发现胶结作用是对断裂带内孔隙度的降低起到主导作用。胶结物的主要来源有两个：断裂带内可溶解的矿物和沿着断裂面侵入的流体。因此，当断层岩发生胶结且具有很好连续性时，断层侧向封闭能力极强，可侧向遮挡油气聚集成藏。 



## 断层侧向封闭机理及封闭类型

断层侧向封闭的本质是断裂带与围岩之间的差异渗透能力（Fisher Q J和Knipe R J），即排替压力差，当断裂带或对盘的排替压力大于储层排替压力时，断层将阻止储层中的油气向围岩侧向运移。反之，当断裂带或对盘的排替压力小于储层排替压力时，断层侧向开启，油气将穿越断层向围岩侧向运移。

![](断裂带封闭油气机理模式图.jpg)

从引起差异渗透的因素来看，断层封闭可以划分为三大类型：

* 对接封闭（Juxtaposition seal），这种封闭与过断层不同岩性对接有关，例如砂泥岩对接或是砂岩与胶结单元对接；
* 断层岩封闭（Fault rock seal），这种封闭与变形过程中断层岩演变类型有关。他们的演变主要取决于母岩的岩性、变形过程、环境以及有关的胶结作用。这种封闭可以分为四个类型：
  * 泥岩涂抹封闭（Clay smear seal）:这是用来描述变形所导致的泥岩或层状硅酸盐涂抹的术语。Lindsay等（1993）识别出三种类型的泥岩涂抹：a.通过砂岩的磨蚀运动；b.上下盘之间页岩层剪切和塑性变形；c.流体化作用过程中（fluidisation）泥岩的注入。Knipe（1992a）和Gibson（1994）发现含有层状硅酸盐或富含层状硅酸盐薄层的非纯砂岩的变形也可以产生泥岩涂抹，并且强调不连续的页岩层是形成泥岩涂抹的必要条件
  * 碎裂岩封闭（Cataclastic rock seal）（Knipe R J，1992a；Knipe R J，1992b），这种断层岩的演化主要受裂缝控制。根据成岩阶段可以将碎屑岩分成三类：a.弱成岩的碎屑岩，没有明显的断裂作用后的压实和胶结。b.部分成岩的碎屑岩，以有一定的压实和沉淀作用为特征。c.成岩的碎屑岩，通过溶解/沉积作用颗粒相互连结。成岩阶段受颗粒大小、压溶作用、变形后石英胶结、变形环境（温度、压力）、变形时间控制；
  * 层状硅酸盐-框架断层岩（Phylloslicate-frame rock seal），这种断层岩指不纯净砂岩（框架硅酸盐与层状硅酸盐比率低），其泥岩/层状硅酸盐含量介于15%~40%。这种岩石由于剪切合并、泥岩/层状硅酸盐涂抹和混合、断裂作用晚期或断裂作用后压溶作用增强、破碎作用、新的层状硅酸盐沉淀形成封闭；
* 胶结封闭（Cemented seal），断裂在变形过程中可以充当流体通道，因此很容易在断裂处沉积成矿。此外，细粒碎屑岩较高的质量传递率使得断裂带在局部溶解和沉积过程中形成“自封”。

上述各种封闭类型都与一套不同的变形过程和封闭机理相关

![断层侧向封闭类型及封闭模式](断层侧向封闭类型及封闭模式.png)

