site_name: FAPSeal_3D断层圈闭密封性评价系统
copyright: 版权所有 &copy; 2019-<script>document.write(new Date().getFullYear())</script> <a href='http://www.faps.com.cn'>黑龙江省飞谱思能源科技有限公司</a> #| <a href='https://beian.miit.gov.cn/'>黑ICP备2023015718号-2</a> # |  基于<a href='https://squidfunk.github.io/mkdocs-material/'>Material for MkDocs</a>构建
#版权所有 &copy; 2023 黑龙江省飞谱思能源科技有限公司

# repo_url: https://gitlab.com/hlKty86/fapsealwebhelp
# edit_uri: -/blob/master/docs

extra: #链接
  #homepage: http://www.faps.com.cn/#/product02/data_manage
  social:
    - icon: simple/bilibili 
      link: https://space.bilibili.com/511172617
    - icon: octicons/home-fill-24 
      link: http://www.faps.com.cn/
    - icon: material/school 
      link: http://www.nepu.edu.cn/
      name: 东北石油大学
  #generator: false



site_url: "" #生成离线文档
use_directory_urls: false #生成离线文档
plugins: 
    - search:
        lang: zh
    - glightbox: #图片放大查看
        auto_caption: true #将图片 alt 用作默认标题
    - privacy: #隐私插件
        enabled: !ENV [CI, false]
    - offline #生成离线文档

    - awesome-pages #自定义导航
    - git-revision-date-localized: #文档编辑日期 #需要建立仓库
        type: iso_date
        enabled: !ENV [CI, false]
    #- print-site: #将打印页面添加到您的网站，该页面结合了整个网站，从而可以轻松导出为 PDF 和独立的 HTML ：https://github.com/timvink/mkdocs-print-site-plugin
    #    add_cover_page: true
    #    cover_page_template: "docs/assets/templates/custom_cover_page.tpl"
    #    add_print_site_banner: true
    #    print_site_banner_template: "docs/assets/templates/custom_banner.tpl"
    #    exclude:
    #      - index.md
    #      - 原理/*
    #      - 帮助与支持/*
    #- blog:
    #    enabled: !ENV [CI, false] 
theme: 
  language: zh
  name: material #shadocs # manuals-theme #windmill #gitbook # material #readthedocs

  logo: assets/logo.png
  favicon: assets/logo.png

  custom_dir: overrides #自定义首页

  font: false


  features: #https://squidfunk.github.io/mkdocs-material/setup/setting-up-navigation/#sticky-navigation-tabs
    - navigation.tabs #启用导航选项卡
    #- navigation.tabs.sticky #导航选项卡向下滚动时始终保持可见
    #- navigation.sections # 侧边栏分组显示
    #- navigation.expand # 展开所有分组
    - navigation.indexes #启用节索引页
    - toc.follow #启用目录的锚点跟随
    - navigation.top #返回顶部按钮
    #- header.autohide #标头会自动隐藏
    - navigation.footer #下一页
    #- toc.integrate #导航集成
    #- content.action.edit
  palette: 

    # Palette toggle for light mode
    - scheme: default
      toggle:
        icon: material/brightness-7 
        name: 点击切换到夜间模式

    # Palette toggle for dark mode
    - scheme: slate
      toggle:
        icon: material/brightness-4
        name: 点击切换到白天模式


markdown_extensions:  
  - toc:
      permalink: "#"
  - attr_list
  - md_in_html
  - tables
  - meta
  - pymdownx.keys #启用显示键盘按键
# 启用内容选项卡
  - pymdownx.superfences
  - pymdownx.tabbed:
      alternate_style: true 
# 启用告示
  - admonition
  - pymdownx.details
  - pymdownx.superfences
# 代码块支持
  - pymdownx.highlight:
      anchor_linenums: true
  - pymdownx.inlinehilite
  - pymdownx.snippets
  - pymdownx.superfences:
      custom_fences:
        - name: mermaid
          class: mermaid
          format: !!python/name:pymdownx.superfences.fence_code_format
# 特定格式支持
  - pymdownx.critic
  - pymdownx.caret
  - pymdownx.keys
  - pymdownx.mark
  - pymdownx.tilde
  - pymdownx.arithmatex: #LATEX公式支持
      generic: true
  - pymdownx.emoji:
      emoji_index: !!python/name:material.extensions.emoji.twemoji
      emoji_generator: !!python/name:material.extensions.emoji.to_svg
      options:
        custom_icons:
          - overrides/.icons 
  - attr_list
  - md_in_html 
    
extra_javascript: 
  #MathJax公式支持
  #- javascripts/mathjax.js
  #- https://polyfill.io/v3/polyfill.min.js?features=es6
  #- https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js
  #KaTeX公式支持
  - javascripts/katex.js
  - https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.16.7/katex.min.js
  - https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.16.7/contrib/auto-render.min.js

extra_css:
  - https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.16.7/katex.min.css


nav:
    - 首页: index.md
    - 使用手册:
      - 使用说明/简介.md
      - 基础功能:
        - ... | flat | 使用说明/01.基础功能/*.md
      - 数据管理:
        - ... | flat | 使用说明/02.数据管理/*.md
      - 井浏览器:
        - ... | flat | 使用说明/03.井浏览器/*.md
      - 平面图浏览器:
        - ... | flat | 使用说明/09.平面图浏览器/*.md
      - 3D模拟器:
        - ... | flat | 使用说明/10.三维浏览器/基本操作.md 
        - ... | flat | 使用说明/10.三维浏览器/构造建模.md 
        - ... | flat | 使用说明/10.三维浏览器/属性评价.md 
        - ... | flat | 使用说明/10.三维浏览器/数据筛选.md 
        - ... | flat | 使用说明/10.三维浏览器/*.md 
      - 三角图模拟器:
        - ... | flat | 使用说明/04.三角图浏览器/*.md
      - 2D断层封闭性:
        - ... | flat | 使用说明/05.二维断层面浏览器/*.md
      - 地质力学分析:
        - ... | flat | 使用说明/06.断层稳定性评价/*.md
#      - 智能圈闭评价:
#        - ... | flat | 使用说明/07.智能圈闭评价/*.md
      - 数据分析:
        - ... | flat | 使用说明/08.数据分析/*.md
    - 原理:
      - 原理/原理.md
      - 断层侧向封闭能力:
        - 原理/断层侧向封闭能力/断层侧向封闭原理.md
        - 原理/断层侧向封闭能力/断层岩封闭能力影响因素分析.md
        - 原理/断层侧向封闭能力/断层侧向封闭评价方法.md
      - 断层稳定性:
        - 原理/断层稳定性/岩石破裂准则.md
        - 原理/断层稳定性/断层稳定性原理及影响因素.md
        - 原理/断层稳定性/断层稳定性评价方法.md
      - 盖层毛细管封闭能力:
        - 原理/盖层毛细管封闭能力/盖层毛细管封闭能力机理.md
        - 原理/盖层毛细管封闭能力/盖层毛细管封闭能力评价方法.md
      - 盖层水力封闭能力:
        - 原理/盖层水力封闭能力/盖层水力封闭能力机理.md
        - 原理/盖层水力封闭能力/盖层水力封闭能力评价方法.md
      - 原理/其他.md
    - 帮助与支持:
      -  ... | flat | 帮助与支持/关于我们.md
      -  ... | flat | 帮助与支持/*.md

 #   - 相关动态:
  #    - blog/index.md 
    
    - 前往主页: http://www.faps.com.cn/#/product02/data_manage









